﻿using ConsoleApp.classes;
using System.Linq.Expressions;
using System.Text;


namespace ConsoleApp
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.InputEncoding = Encoding.UTF8;
            Console.OutputEncoding = Encoding.UTF8;
            string str = "Hello, World";
            Console.WriteLine("Інвертування рядка: ");

            Console.WriteLine(str.Invert());

            const char symbol = 'l';
            Console.WriteLine($"Підрахунок кількості входжень символа {symbol} в рядок: ");
            Console.WriteLine(str.CountChar(symbol));
        }
    }
}
