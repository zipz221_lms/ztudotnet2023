﻿using ClassLibrary;
//
namespace ConsoleApp
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Bank bank = new Bank();
            Account account1 = new Account(123456, 1234, 1000);
            Account account2 = new Account(987654, 4321, 2000);
            bank.AddAccount(account1);
            bank.AddAccount(account2);

            AutomatedTellerMachine atm = new AutomatedTellerMachine(bank);

           
            atm.AuthenticationEvent += AuthenticationHandler;
            atm.BalanceInquiryEvent += BalanceInquiryHandler;
            atm.WithdrawalEvent += WithdrawalHandler;
            atm.DepositEvent += DepositHandler;
            atm.TransferEvent += TransferHandler;
            atm.ErrorEvent += ErrorHandler;

            // Example operations
            try
            {
                atm.Authenticate(123456, 1234); // Correct credentials
                atm.CheckBalance(1234256); // INCorrect credentials
                atm.Withdraw(123456, 500);
                atm.Deposit(123456, 200);
                Console.Write("Account 987654 balance: ");
                atm.CheckBalance(987654);
                atm.Transfer(123456, 987654, 300);
                atm.CheckBalance(987654);
            }
            catch (UnauthorizedAccessException ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadLine(); 
        }
        static void AuthenticationHandler(bool success)
        {
            if (success)
                Console.WriteLine("Authentication successful.");
            else
                Console.WriteLine("Authentication failed.");
        }

        static void BalanceInquiryHandler(decimal balance)
        {
            if (balance >= 0)
                Console.WriteLine($"Current balance: {balance}");
            else
                Console.WriteLine("Account not found.");
        }

        static void WithdrawalHandler(bool success)
        {
            if (success)
                Console.WriteLine("Withdrawal successful.");
            else
                Console.WriteLine("Withdrawal failed due to insufficient balance.");
        }

        static void DepositHandler()
        {
            Console.WriteLine("Deposit successful.");
        }

        static void TransferHandler(bool success)
        {
            if (success)
                Console.WriteLine("Transfer successful.");
            else
                Console.WriteLine("Transfer failed due to insufficient balance or invalid account.");
        }

        static void ErrorHandler(string errorMessage)
        {
            Console.WriteLine($"Error: {errorMessage}");
        }
    }
}
