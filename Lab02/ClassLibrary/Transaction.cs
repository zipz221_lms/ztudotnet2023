﻿

namespace ClassLibrary
{
    public class Transaction
    {
        public Guid TransactionID { get; }
        public string Type { get; }
        public decimal Amount { get; }

        public Transaction(string type, decimal amount)
        {
            TransactionID = Guid.NewGuid();
            Type = type;
            Amount = amount;
        }
    }
}
