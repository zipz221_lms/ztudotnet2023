﻿

namespace ClassLibrary
{
    public class Bank
    {
        private List<Account> accounts = new List<Account>();
        private List<Transaction> transactions = new List<Transaction>();

        public void AddAccount(Account account)
        {
            accounts.Add(account);
        }

        public Account GetAccount(int accountNumber)
        {
            return accounts.Find(acc => acc.AccountNumber == accountNumber);
        }

        public void AddTransaction(Transaction transaction)
        {
            transactions.Add(transaction);
        }
    }
}
