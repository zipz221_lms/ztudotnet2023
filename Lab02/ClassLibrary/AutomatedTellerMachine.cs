﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace ClassLibrary
{
    public delegate void AuthenticationEventHandler(bool success);
    public delegate void BalanceInquiryEventHandler(decimal balance);
    public delegate void WithdrawalEventHandler(bool success);
    public delegate void DepositEventHandler();
    public delegate void TransferEventHandler(bool success);
    public delegate void ErrorEventHandler(string errorMessage);

   
    public class AutomatedTellerMachine
    {
        private Bank bank;
        private bool isAuthenticated;

       
        public event AuthenticationEventHandler AuthenticationEvent;
        public event BalanceInquiryEventHandler BalanceInquiryEvent;
        public event WithdrawalEventHandler WithdrawalEvent;
        public event DepositEventHandler DepositEvent;
        public event TransferEventHandler TransferEvent;
        public event ErrorEventHandler ErrorEvent;

        public AutomatedTellerMachine(Bank bank)
        {
            this.bank = bank;
        }
        private void CheckAuthorization()
        {
            if (!isAuthenticated)
            {
               
                throw new UnauthorizedAccessException("Unauthorized access.");
            }
        }
        public void Authenticate(int accountNumber, int pin)
        {
            Account account = bank.GetAccount(accountNumber);
            bool success = account != null && account.PIN == pin;
            if (success)
            {
                isAuthenticated = true;
            }
            AuthenticationEvent?.Invoke(success);
          
        }

        public void CheckBalance(int accountNumber)
        {
            CheckAuthorization();
            Account account = bank.GetAccount(accountNumber);
            decimal balance = account != null ? account.Balance : -1; // Return -1 if account not found
            BalanceInquiryEvent?.Invoke(balance);
        }
        

        public void Withdraw(int accountNumber, decimal amount)
        {
            CheckAuthorization();

            Account account = bank.GetAccount(accountNumber);
            if (account == null)
            {
                ErrorEvent?.Invoke("Invalid account number.");
                return;
            }

            if (account.Balance < amount)
            {
                ErrorEvent?.Invoke("Insufficient funds.");
                return;
            }

            account.UpdateBalance(-amount);
            bank.AddTransaction(new Transaction("Withdrawal", amount));
            WithdrawalEvent?.Invoke(true);
        }

        public void Deposit(int accountNumber, decimal amount)
        {
            CheckAuthorization();
            Account account = bank.GetAccount(accountNumber);
            if (account == null)
            {
                ErrorEvent?.Invoke("Invalid account number.");
                return;
            }

            account.UpdateBalance(amount);
            bank.AddTransaction(new Transaction("Deposit", amount));
            DepositEvent?.Invoke();
        }

        public void Transfer(int fromAccountNumber, int toAccountNumber, decimal amount)
        {
            CheckAuthorization();
            Account fromAccount = bank.GetAccount(fromAccountNumber);
            Account toAccount = bank.GetAccount(toAccountNumber);

            if (fromAccount == null || toAccount == null)
            {
                ErrorEvent?.Invoke("Invalid account number.");
                return;
            }

            if (fromAccount.Balance < amount)
            {
                ErrorEvent?.Invoke("Insufficient funds.");
                return;
            }

            fromAccount.UpdateBalance(-amount);
            toAccount.UpdateBalance(amount);
            bank.AddTransaction(new Transaction("Transfer", amount));
            TransferEvent?.Invoke(true);
        }
    }
}
   
