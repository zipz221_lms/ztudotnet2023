﻿
namespace ClassLibrary
{
    public class Account
    {
        public int AccountNumber { get; }
        public int PIN { get; }
        public decimal Balance { get; private set; }

        public Account(int accountNumber, int pin, decimal balance)
        {
            AccountNumber = accountNumber;
            PIN = pin;
            Balance = balance;
        }

        public void UpdateBalance(decimal amount)
        {
            Balance += amount;
        }
    }
}
