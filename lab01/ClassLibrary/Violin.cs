﻿
namespace ClassLibrary
{
    public class Violin : MusicalInstrument
    {
        public Violin() : base() { }

        public Violin(string name, string characteristics, string history) : base(name, characteristics, history) { }

        public Violin(Violin original) : base(original) { }

        public override void Sound()
        {
            Console.WriteLine("Violin sound");
        }
    }
}
