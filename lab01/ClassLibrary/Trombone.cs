﻿
namespace ClassLibrary
{
    public class Trombone : MusicalInstrument
    {
        public Trombone() : base() { }

        public Trombone(string name, string characteristics, string history) : base(name, characteristics, history) { }

        public Trombone(Trombone original) : base(original) { }

        public override void Sound()
        {
            Console.WriteLine("Trombone sound");
        }
    }
}
