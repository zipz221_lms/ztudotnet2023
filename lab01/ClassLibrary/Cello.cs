﻿
namespace ClassLibrary
{
    public class Cello : MusicalInstrument
    {
        public Cello() : base() { }

        public Cello(string name, string characteristics, string history) : base(name, characteristics, history) { }

        public Cello(Cello original) : base(original) { }

        public override void Sound()
        {
            Console.WriteLine("Cello sound");
        }
    }
}
