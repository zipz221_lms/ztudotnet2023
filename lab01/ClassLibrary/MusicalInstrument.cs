﻿namespace ClassLibrary
{
    public abstract class MusicalInstrument
    {
        protected string name;
        protected string characteristics;
        protected string history;

        public MusicalInstrument()
        {
            this.name = "Unknown";
            this.characteristics = "Unknown characteristics";
            this.history = "Unknown";
        }

        public MusicalInstrument(string name, string characteristics,string history)
        {
            this.name = name;
            this.characteristics = characteristics;
            this.history = history;
        }

        public MusicalInstrument(MusicalInstrument original)
        {
            this.name = original.name;
            this.characteristics = original.characteristics;
            this.history = original.history;
        }
        
        public void SetName(string name)
        {
            this.name = name;
        }

        public string GetName()
        {
            return this.name;
        }

        public void SetCharacteristics(string characteristics)
        {
            this.characteristics = characteristics;
        }

        public string GetCharacteristics()
        {
            return this.characteristics;
        }
        public string SetHistory()
        {
            return this.history;
        }
        public string GetHistory()
        {
            return this.history;
        }

        public abstract void Sound();

        public void Show()
        {
            Console.WriteLine($"Musical Instrument: {name}");
        }

        public void Desc()
        {
            Console.WriteLine($"Characteristics: {characteristics}");
        }

        public void History()
        {
            Console.WriteLine($"History of {name}: {history}.");
        }

        public void ShowInfo()
        {
            Show();
            Desc();
            History();
        }
    }
}
