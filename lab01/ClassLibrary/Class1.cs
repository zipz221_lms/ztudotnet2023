﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public abstract class MusicalInstrument
    {
        protected string name;
        protected string characteristics;
        protected string history;

        public MusicalInstrument()
        {
            this.name = "Unknown";
            this.characteristics = "Unknown characteristics";
            this.history = "Unknown";
        }

        public MusicalInstrument(string name, string characteristics, string history)
        {
            this.name = name;
            this.characteristics = characteristics;
            this.history = history;
        }

        public MusicalInstrument(MusicalInstrument original)
        {
            this.name = original.name;
            this.characteristics = original.characteristics;
            this.history = original.history;
        }

        public void SetName(string name)
        {
            this.name = name;
        }

        public string GetName()
        {
            return this.name;
        }

        public void SetCharacteristics(string characteristics)
        {
            this.characteristics = characteristics;
        }

        public string GetCharacteristics()
        {
            return this.characteristics;
        }
        public string SetHistory()
        {
            return this.history;
        }
        public string GetHistory()
        {
            return this.history;
        }

        public abstract void Sound();

        public void Show()
        {
            Console.WriteLine($"Musical Instrument: {name}");
        }

        public void Desc()
        {
            Console.WriteLine($"Characteristics: {characteristics}");
        }

        public void History()
        {
            Console.WriteLine($"History of {name}: {history}.");
        }

        public void ShowInfo()
        {
            Show();
            Desc();
            History();
        }
    }
    public class Cello : MusicalInstrument
    {
        public Cello() : base() { }

        public Cello(string name, string characteristics, string history) : base(name, characteristics, history) { }

        public Cello(Cello original) : base(original) { }

        public override void Sound()
        {
            Console.WriteLine("Cello sound");
        }
    }
    public class Trombone : MusicalInstrument
    {
        public Trombone() : base() { }

        public Trombone(string name, string characteristics, string history) : base(name, characteristics, history) { }

        public Trombone(Trombone original) : base(original) { }

        public override void Sound()
        {
            Console.WriteLine("Trombone sound");
        }
    }
    public class Ukulele : MusicalInstrument
    {
        public Ukulele() : base() { }

        public Ukulele(string name, string characteristics, string history) : base(name, characteristics, history) { }

        public Ukulele(Ukulele original) : base(original) { }

        public override void Sound()
        {
            Console.WriteLine("Ukulele sound");
        }
    }
    public class Violin : MusicalInstrument
    {
        public Violin() : base() { }

        public Violin(string name, string characteristics, string history) : base(name, characteristics, history) { }

        public Violin(Violin original) : base(original) { }

        public override void Sound()
        {
            Console.WriteLine("Violin sound");
        }
    }
}
