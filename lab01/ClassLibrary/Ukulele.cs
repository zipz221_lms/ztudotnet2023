﻿
namespace ClassLibrary
{
    public class Ukulele : MusicalInstrument
    {
        public Ukulele() : base() { }

        public Ukulele(string name, string characteristics, string history) : base(name, characteristics, history) { }

        public Ukulele(Ukulele original) : base(original) { }

        public override void Sound()
        {
            Console.WriteLine("Ukulele sound");
        }
    }
}
