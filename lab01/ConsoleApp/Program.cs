﻿using ClassLibrary;

namespace ConsoleApp
{
    internal class Program
    {
        static void Main(string[] args)
        {
            MusicalInstrument[] instruments = new MusicalInstrument[]
         {
            new Violin("Violin", "String instrument", "Violin was invented in the early 16th century"),
            new Trombone("Trombone", "Brass instrument", "Trombone has a long history, dating back to the Renaissance"),
            new Ukulele("Ukulele","String instrument", "Ukulele originated in the 19th century in Hawaii"),
            new Cello("Cello", "String instrument", "Cello has a history dating back to the 16th century.")
         };

            foreach (var instrument in instruments)
            {
                instrument.Sound();
                instrument.ShowInfo();
              
                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }
}
